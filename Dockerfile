FROM python:3.11.6-alpine3.18

EXPOSE 5000

RUN apk add build-base git
RUN apk add libffi-dev
RUN mkdir /app
WORKDIR /app

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY secrets.yaml ./
COPY templates/ ./templates/

COPY app.py ./
CMD quart --debug run --host '0.0.0.0'
