import quart
import types
import nacl.pwhash
import quart_jwt_extended as jwt
import jwt as jwt_legacy
import functools
import asyncio
import aiohttp
import json
import base64
import yaml
import dataclasses
import urllib.parse
import secrets as secure_random
from quart import g as request_context
import re
import nats
import nats_json_rpc

with open('secrets.yaml', 'r') as secrets_file:
    secrets = yaml.load(secrets_file, yaml.Loader)

app = quart.Quart(__name__)
app.config['JWT_SECRET_KEY'] = secrets['jwt']['secret']
app.config['JWT_TOKEN_LOCATION'] = ['headers', 'cookies']
app.config['JWT_DECODE_ALGORITHMS'] = ['HS256', 'RS256']

@dataclasses.dataclass
class OAuthBackend:
    client_id: str
    url_authorize: str
    url_token: str
    scope: str
    url_user: str = None

oauth_backends = {
    'google': OAuthBackend(
        secrets['google']['client_id'],
        'https://accounts.google.com/o/oauth2/v2/auth',
        'https://oauth2.googleapis.com/token',
        'openid'
    ), 
    'microsoft': OAuthBackend(
        secrets['microsoft']['client_id'],
        'https://login.microsoftonline.com/common/oauth2/v2.0/authorize',
        'https://login.microsoftonline.com/common/oauth2/v2.0/token',
        'openid'
    ), 
    'github': OAuthBackend(
        secrets['github']['client_id'],
        'https://github.com/login/oauth/authorize',
        'https://github.com/login/oauth/access_token',
        'read_user',
        'https://api.github.com/user'
    ), 
    'gitlab': OAuthBackend(
        secrets['gitlab']['client_id'],
        'https://gitlab.com/oauth/authorize',
        'https://gitlab.com/oauth/token',
        'openid'
    )
}

@app.before_serving
async def nats_open():
    app.nats = types.SimpleNamespace()
    app.nats.connection = await nats.connect('nats://nats')
    app.nats.rpc        = nats_json_rpc.NatsJsonRPC(app.nats.connection, 'structure.rpc.')
    app.nats.rpc.rpc_register(access_token_generate)
    app.nats.rpc.rpc_register(auth_user_get)
    await app.nats.rpc.rpc_init()
    
    app.jwt = jwt.JWTManager(app)
    app.client = types.SimpleNamespace()
    app.client.session = aiohttp.ClientSession()


def request_auth_response(func):
    @functools.wraps(func)
    async def replacement(*args, **kwargs):
        result = await func(*args, **kwargs)
        if result == True:
            return {'result': True}

        if result == False:
            return quart.Response('Not allowed', 403)

        return result

    return replacement


def request_forward(base='http://worker-api-nodes:5000'):
    def decorator_request_forward(func):
        @functools.wraps(func)
        async def replacement(*args, **kwargs):
            result = await func(*args, **kwargs)
            if not result:
                return quart.Response('Not allowed', 403)

            response = await app.client.session.request(
                quart.request.method,
                f'{base}{quart.request.full_path}',
                headers=quart.request.headers,
                data=await quart.request.body
            )

            headers = {
                key: value 
                for 
                key, value 
                in response.headers.items()
                if not key.lower() in ['server', 'date']    
            }

            return quart.Response(
                await response.text(),
                response.status,
                headers
            )

        return replacement
    return decorator_request_forward


def inject_node(node_id_param='node_id', is_list=False, include_attributes=False, parse_includes=False):
    def decorator_inject_node(func):
        @functools.wraps(func)
        @timeout()
        async def replacement(*args, **kwargs):
            _is_list = is_list
            if _is_list:
                node_id = quart.request.args['id']
            else:
                node_id = kwargs[node_id_param]
                if node_id == 'multiple':
                    node_id = quart.request.args['id']
                    _is_list = True

            include_children = False

            if parse_includes:
                includes = quart.request.args.getlist('include')
                include_children = 'children' in includes

            if include_children and not user_has_permission('children'):
                return False

            node = None

            kwargs.pop(node_id_param, None)

            if _is_list:
                nodes = await app.nats.rpc.proxy.nodes_get(
                    _id=node_id,
                    recursive=include_children,
                    include_attributes=include_attributes
                )
                node = nodes[0]
            else:
                node = await app.nats.rpc.proxy.node_get(
                    _id=node_id,
                    include_attributes=include_attributes
                )

            # we check for this since our RPC channel cannot deliver None directly
            if node == {}:
                node = None
            if node == None:
                # while 404 would be the appropriate code,
                # our auth context in nginx converts that to 500, which is rather useless...
                return f'Node "{node_id}" not found', 403

            return await func(node=node, *args, **kwargs)

        return replacement
    return decorator_inject_node

def timeout(timeout=5):
    def decorator_timeout(func):
        @functools.wraps(func)
        async def replacement(*args, **kwargs):
            try:
                async with asyncio.timeout(timeout):
                    return await func(*args, **kwargs)
            except asyncio.TimeoutError:
                return 'Backend timed out', 504

        return replacement

    return decorator_timeout

async def auth_user_get(request=None, args=None, headers=None, cookies=None):
        if request is not None:
            args = request.args
            headers = request.headers
            cookies = request.cookies

        token = cookies.get('token')

        if token is None:
            try:
                header = headers['Authorization']
                token = re.findall('[a-z0-9]{64}', header)[0]
            except KeyError:
                pass
            except IndexError:
                return 'Token malformed', 401

        if token is None:
            token = args.get('token')

        if token is None:
            return 'Missing token', 401

        return await app.nats.rpc.proxy.users_get_by_token(token=token)

def token_required(func):
    @functools.wraps(func)
    async def replacement(*args, **kwargs):
        user = await auth_user_get(quart.request)

        if isinstance(user, tuple):
            # method returned an error / code combination
            return user

        # only required if token is from cookie
        # also only check if not GET since GET should not change anything serverside
        csrf_validation_needed = (quart.request.method != 'GET') and (quart.request.cookies.get('token') is not None)

        if csrf_validation_needed:
            expected_csrf_token = user['csrf_token']
            actual_csrf_token = quart.request.headers.get('x-csrf-token')
            if expected_csrf_token != actual_csrf_token:
                print('CSRF validation failed')
                return 'CSRF validation failed', 401

        request_context.user = user

        return await func(*args, **kwargs)

    return replacement


def user_get_claims():
    return request_context.user

def user_get_root_node():
    claims = user_get_claims()
    return claims['rootNode']

def user_has_permission(permission):
    claims = user_get_claims()
    return claims['permissions'].get(permission, False)


def require_permission_node(*permissions):
    def decorator(func):
        @functools.wraps(func)
        async def replacement(*args, **kwargs):
            for permission in permissions:
                if not user_has_permission(permission):
                    return False
            rootNode = user_get_root_node()

            node = kwargs['node']
            if type(node) == 'list':
                node = node[0]

            if node['_id'] == rootNode['id']:
                # user is accessing his root node, no need to check if child
                return await func(*args, **kwargs)

            if not user_has_permission('children'):
                # user cannot access children anyway, no need to check for parentship
                return False

            if node['parentPath'].startswith(f'{rootNode["parentPath"]}/{rootNode["id"]}'):
                return await func(*args, **kwargs)

            return False
        return replacement
    return decorator

@app.get('/api/nodes/<string:node_id>')
@token_required
@inject_node()
@request_auth_response
@require_permission_node('node_read')
async def node_get(node: dict):
    return True

@app.put('/api/nodes/<string:node_id>')
@app.patch('/api/nodes/<string:node_id>')
@app.delete('/api/nodes/<string:node_id>')
@token_required
@inject_node()
@request_auth_response
@require_permission_node('node_write')
async def node_write(node: dict):
    return True

@app.get('/api/nodes/<string:node_id>/attributes/<string:attribute_name>')
@app.get('/api/nodes/<string:node_id>/attributes')
@token_required
@inject_node()
@request_auth_response
@require_permission_node('attributes_read')
async def node_attributes_read(node: dict, attribute_name: str=None):
    return True

@app.get('/api/socket.io/socket.io.js')
@request_auth_response
async def socket_io_js():
    return True

@app.route('/api/socket.io/', methods=['GET', 'POST'])
@token_required
@inject_node(is_list=True)
@request_auth_response
@require_permission_node('attributes_read', 'node_read')
async def socket_io(node: dict):
    if 'children' in quart.request.args.getlist('include'):
        if not user_has_permission('children'):
            return False

    return True

@app.get('/api/nodes/<string:node_id>/updates')
@token_required
@inject_node()
@request_auth_response
@require_permission_node('attributes_read', 'node_read')
async def node_updates(node: dict):
    if 'children' in quart.request.args.getlist('include'):
        if not user_has_permission('children'):
            return False

    return True

@app.put('/api/nodes/<string:node_id>/attributes/<string:attribute_name>')
@app.post('/api/nodes/<string:node_id>/attributes/<string:attribute_name>')
@app.post('/api/nodes/<string:node_id>/attributes')
@app.patch('/api/nodes/<string:node_id>/attributes')
@token_required
@inject_node()
@request_auth_response
@require_permission_node('attributes_write')
async def node_attributes_write(node: dict, attribute_name: str=None):
    return True

@app.get('/api/nodes')
@token_required
@inject_node(is_list=True, parse_includes=True)
@request_auth_response
@require_permission_node('node_read')
async def nodes_list(node: dict):
    if 'children' in quart.request.args.getlist('include'):
        if not user_has_permission('children'):
            return False
    if 'attributes' in quart.request.args.getlist('include'):
        if not user_has_permission('attributes_read'):
            return False
        
    return True

@app.post('/api/nodes/<string:node_id>/children')
@token_required
@inject_node()
@request_auth_response
@require_permission_node('node_write', 'children')
async def nodes_create(node: dict):
    return True

@app.get('/api/nodes/<string:node_id>/accounts')
@app.post('/api/nodes/<string:node_id>/accounts')
@app.delete('/api/nodes/<string:node_id>/accounts/<string:account_id>')
@token_required
@inject_node()
@request_auth_response
@require_permission_node('accounts')
async def node_accounts(node: dict, account_id: str = None):
    return True

@app.get('/api/nodes/<string:node_id>/accounts/<string:account_id>/token')
@token_required
@inject_node()
@request_auth_response
@require_permission_node('accounts')
async def account_key(node: dict, account_id: str):
    account = await app.nats.rpc.proxy.users_get_by_id(account_id=account_id)

    if account is None:
        return 'Account not found', 404

    accountRoot = account['rootNode']
    accountPath = f'{accountRoot["parentPath"]}/{str(accountRoot["id"])}'

    nodePath = f'{node["parentPath"]}/{node["_id"]}'

    if accountPath.startswith(nodePath):
        return True

    return False

@app.get('/api/auth/me')
@token_required
async def auth_me():
    return {key: request_context.user.get(key) for key in ['_id', 'permissions', 'rootNode', 'csrf_token']}

async def access_token_generate(account_id):
    account = await app.nats.rpc.proxy.users_get_by_id(account_id=account_id)

    return account['token']

    if account is None:
        return 'Account not found'

    claims = {
        'permissions': account['permissions'],
        'rootNode': account['rootNode'],
    }

    return jwt.create_access_token(identity=None, user_claims=claims, expires_delta=False)

async def oauth_username_get(code, backend_id, endpoint):
    backend = oauth_backends[backend_id]
    response = await app.client.session.post(
        backend.url_token,
        data={
            'code': code,
            'redirect_uri': f'https://structure.nullco.de/api/auth/{endpoint}',
            'grant_type': 'authorization_code'
        } | secrets[backend_id],
        headers={
            'Accept': 'application/json'
        }
    )

    if response.status != 200:
        raise RuntimeError(f'OAuth backend code not 200 ({response.status})')

    response = await response.json()

    if 'id_token' in response.keys():
        id_token = response['id_token']
        id = jwt_legacy.decode(id_token, options=dict(verify_signature=False))
        return id['sub']

    token = response['access_token']
    response = await app.client.session.get(
        backend.url_user, 
        headers={
        'Authorization': f'Bearer {token}'
    })
    if response.status != 200:
        raise RuntimeError(f'Github user endpoint status not 200 ({response.status})')

    response = await response.json()
    return response['id']

async def login_user(user: dict):
    """
    claims = {
        'permissions': user['permissions'],
        'rootNode': user['rootNode'],
    }

    access_token = jwt.create_access_token({
        'username': user['username']
    }, user_claims=claims,
    expires_delta=False)

    response = quart.redirect(
        f'/#{user["rootNode"]["id"]}'
    )

    jwt.set_access_cookies(
        response, 
        access_token
    )

    user_serialized = json.dumps(claims)
    user_encoded = base64.b64encode(user_serialized.encode())

    """
    try:
        user['token']
    except KeyError:
        user['token'] = secure_random.token_hex()
        user['csrf_token'] = secure_random.token_hex()
        await app.nats.rpc.proxy.users_update(user=user)

    claims = {
        'permissions': user['permissions'],
        'rootNode': user['rootNode'],
    }
    user_serialized = json.dumps(claims)
    # user_encoded = base64.b64encode(user_serialized.encode())
    # target_node_id = user["rootNode"]["id"]
    transmitted_node_id = (await quart.request.form).get('node_id')
    if transmitted_node_id:
        response = quart.redirect(
            f'/?node_id={transmitted_node_id}'
        )
    else:
        response = quart.redirect(
            f'/'
        )

    response.set_cookie('token', user['token'], httponly=True)
    # disabled, since the client makes a call to /auth/me anyways to retrieve the csrf_token and user
    # response.set_cookie('csrf_token', user['csrf_token'])

    return response

@app.get('/api/auth/register')
async def auth_register_oauth():
    params = quart.request.args
    state = params['state']
    state = base64.b64decode(state)
    state = json.loads(state)

    invitation_id = state['invitation']

    code = params['code']

    backend = state['type']
    
    if backend in oauth_backends.keys():
        username = await oauth_username_get(code, backend, 'register')
    else:
        raise RuntimeError(f'Auth backend {backend} not found')

    invited_user = await app.nats.rpc.proxy.users_get_by_invitation(invitation_id=invitation_id)

    if invited_user is None:
        return 'Invitation not found'

    invited_user['type'] = 'user'
    invited_user['backend'] = backend
    invited_user['username'] = username
    invited_user['token'] = secure_random.token_hex()

    await app.nats.rpc.proxy.users_update(user=invited_user)

    return await login_user(invited_user)


@app.post('/api/auth/register')
async def auth_register():
    form = await quart.request.form

    invitation_id = form.get('invitation', '')
    username = form.get('username', '')
    password = form.get('password', '')

    def display_error(error):
        return display_login_page(page='/register', error=error, invitation=invitation_id)

    if invitation_id == '':
        return display_error('Invitation missing')

    if len(username) < 6:
        return display_error('Username must be at least 6 characters long')

    if len(password) < 6:
        return display_error('Password must be at least 6 characters long')

    invited_user = await app.nats.rpc.proxy.users_get_by_invitation(invitation_id=invitation_id)

    if invited_user is None:
        return display_error('Invitation not found')

    password_encoded = password.encode()

    password_hashed = nacl.pwhash.str(password_encoded)

    invited_user['type'] = 'user'
    invited_user['backend'] = 'internal'
    invited_user['username'] = username
    invited_user['password_hash'] = password_hashed.decode()
    invited_user['token'] = secure_random.token_hex()

    await app.nats.rpc.proxy.users_update(user=invited_user)

    return await login_user(invited_user)



@app.get('/api/auth/login')
async def auth_login_oauth():
    params = quart.request.args
    state = params['state']
    state = base64.b64decode(state)
    state = json.loads(state)

    code = params['code']
    backend = state['type']
    
    if backend in oauth_backends.keys():
        username = await oauth_username_get(code, backend, 'login')
    else:
        raise RuntimeError(f'Auth backend {backend} not found')

    saved_user = await app.nats.rpc.proxy.users_get_by_username(username=username, backend=backend)

    if saved_user is None:
        return 'Username or password incorrect', 401

    return await login_user(saved_user)

def display_login_page(page='/login', **kwargs):
    return quart.redirect(f'{page}?{urllib.parse.urlencode(kwargs)}')

@app.post('/api/auth/login')
async def auth_login():
    form = await quart.request.form

    username = form.get('username', '')
    password = form.get('password', '')

    saved_user = await app.nats.rpc.proxy.users_get_by_username(username=username, backend='internal')

    try:
        if saved_user is None:
            raise nacl.exceptions.InvalidkeyError()

        saved_password_hash = saved_user['password_hash']

        verification_result = nacl.pwhash.verify(saved_password_hash.encode(), password.encode())

        if verification_result != True:
            raise nacl.exceptions.InvalidkeyError()

        return await login_user(saved_user)
    except nacl.exceptions.InvalidkeyError:
        return display_login_page(error='Username or password wrong')

@app.get('/register')
@app.get('/login')
async def login_page():
    action=quart.request.path.split('/')[-1]

    if action == 'login' and not await app.nats.rpc.proxy.database_initialized():
        await app.nats.rpc.proxy.database_initialize()
        return display_login_page(
            '/register', 
            message='This is your one-time invitation', 
            invitation='5a8a5505af68e07d0532d1e7ef75d624c94285d05cf6e91fc5fff8a945baad8f'
        )

    backends = []
    params = {
        'redirect_uri': f'https://structure.nullco.de/api/auth/{action}',
        'response_type': 'code'
    }
    invitation = quart.request.args.get('invitation', '')
    state = {}

    if action == 'register':
        state['invitation'] = invitation

    for type, backend in oauth_backends.items():
        state['type'] = type
        params['state'] = base64.b64encode(json.dumps(state).encode()).decode()
        params['client_id'] = backend.client_id
        params['scope'] = backend.scope

        backends.append({
            'type': type, 
            'url': f'{backend.url_authorize}?{urllib.parse.urlencode(params)}'
        })

    return await quart.render_template(
        'login.j2', 
        action=action, 
        backends=backends, 
        invitation=invitation, 
        node_id=quart.request.args.get('node_id'),
        message=quart.request.args.get('message'),
        error=quart.request.args.get('error')
    )